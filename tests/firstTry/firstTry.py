#! /usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
import csv 

fig, ax_lst = plt.subplots(2, 2)

xdata=[]
ydata=[]
with open('firstTry.csv','r') as csvfile:
    data = csv.reader(csvfile, delimiter=' ')
    next(data, None)  # skip the headers
    for row in data:
        xdata.append(int(row[0]))
        ydata.append(int(row[1]))

x = np.linspace(0, 10, 10) 
xfine = np.linspace(0, 10, 100) 

ax_lst[1][1].plot(x,x**2,'b.',label='Quadratisch')
ax_lst[1][1].plot(xdata,ydata,'b*',label='CSV values')
ax_lst[0][1].plot(x,x**3,'y-',label='Kubisch')
ax_lst[1][0].plot(x,x,'g:',label='Linear')
ax_lst[0][0].plot(x,np.sin(x*np.pi),'rx',label='sin')
ax_lst[0][0].plot(xfine,np.sin(xfine*np.pi),'r-',label='sin')
ax_lst[1][0].plot(x,3*x,'m-.',label='Linear 3x')

fig.legend()
fig.show()
input("Press Enter to continue")
fig.savefig('firstTry.eps', bbox_inches='tight')

plt.pause(10)

plt.close(fig)
