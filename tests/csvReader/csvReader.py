#! /usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np
import sys
import csv
import os.path

axisColors = ("r", "g", "b", "y")

# function to print text to stderr
def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

# checks if the argument has been passed to script
if len(sys.argv) != 2:
    eprint(sys.argv[0] + " <csv file>")
    sys.exit(1)

# check if file exists
if not os.path.exists(sys.argv[1]):
    eprint("File does not exist!")
    sys.exit(1)

# parse csv file
with open(sys.argv[1], "r") as currentFile:

    # read content and create interator with values
    csvContent = csv.reader(currentFile, delimiter=",")

    # read headline and create lists for x and y axis
    headLine = next(csvContent, None)
    xData = []
    yData = []
    for iAmUseless in range(len(headLine) - 1):
        yData.append([])

    # paste data from csv into list
    for currentLine in csvContent:
        xData.append(currentLine[0])
        for currentRow in range(len(currentLine) - 1):
            yData[currentRow].append(currentLine[currentRow + 1])
        
# fill in xlabel and write filename without last extension to the title
plt.xlabel(headLine[0])
plt.title(sys.argv[1].replace("." + sys.argv[1].split(".")[len(sys.argv[1].split(".")) - 1], ""))

# fill data into axis
for currentYAxis in range(len(yData)):
    plt.plot(xData, yData[currentYAxis], axisColors[currentYAxis % len(axisColors)] + "-", label=headLine[currentYAxis + 1])

# plot graph, replace the last file ending with eps
plt.legend()
plt.savefig(sys.argv[1].replace("." + sys.argv[1].split(".")[len(sys.argv[1].split(".")) - 1], "") + ".eps", bbox_inches="tight")

sys.exit(0)
