#! /usr/bin/env python

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
import numpy as np

fig = plt.figure()
ax1 = fig.add_subplot(111, projection='3d')

x = np.linspace(0, 100, 100) 

for i in range(1, 7):
    ax1.plot(x,x + i,x,'r-',label='bla')

fig.legend()
fig.show()
input("Press Enter to continue")
plt.close(fig)
