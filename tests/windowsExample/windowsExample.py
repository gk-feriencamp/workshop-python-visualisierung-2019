#! /usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np

minutes = np.linspace(0, 90, 90)
joy = np.linspace(0, 100, 90)

plt.xlabel("Time in m")
plt.ylabel("Joy in %")
plt.title("Some cool graph")

plt.plot(minutes, joy, "g-", label="Joy")

plt.legend()
plt.show()

while True:
    plt.pause(1)

plt.close()
sys.exit(0)
