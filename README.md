# Workshop Python Visualisierung 2019
## To get it working
* install anaconda (https://www.anaconda.com/distribution/)
* docu (https://www.w3schools.com/python/python_functions.asp)
* tutorials (https://matplotlib.org/tutorials/introductory/pyplot.html#sphx-glr-tutorials-introductory-pyplot-py)
* run script in python: exec(open("./firstTry.py").read())
